package com.example.trakk2.utils;

import com.example.trakk2.Model.User;

public class Common {
    public static final String USER_INFORMATION = "UserInformation";
    public static final User LoggedUser = null;
    public static final String USER_ID_SAVE_KEY = "SaveUid";
    public static final String TOKENS = "Tokens";
}
